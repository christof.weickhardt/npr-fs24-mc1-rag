version: '3.8'
services:
  bge-m3:
    image: ghcr.io/huggingface/text-embeddings-inference:cpu-1.2
    command:
      - --model-id=BAAI/bge-m3
      - --payload-limit=200000000
      - --max-client-batch-size=4096
      - --max-batch-tokens=131072
      - --dtype=float16
      - --auto-truncate
    ports:
      - "8080:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  labse:
    image: ghcr.io/huggingface/text-embeddings-inference:cpu-1.2
    command:
      - --model-id=setu4993/LaBSE
      - --payload-limit=2000000000
      - --max-client-batch-size=4096
      - --max-batch-tokens=131072
      - --dtype=float16
      - --pooling=cls
      - --auto-truncate
    ports:
      - "8081:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  nomic-embed-text-v1-5:
    image: ghcr.io/huggingface/text-embeddings-inference:cpu-1.2
    command:
      - --model-id=nomic-ai/nomic-embed-text-v1.5
      - --payload-limit=2000000000
      - --max-client-batch-size=4096
      - --max-batch-tokens=131072
      - --dtype=float16
      - --auto-truncate
    ports:
      - "8082:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  paraphrase-multilingual-mpnet-base-v2:
    image: ghcr.io/huggingface/text-embeddings-inference:cpu-1.2
    command:
      - --model-id=sentence-transformers/paraphrase-multilingual-mpnet-base-v2
      - --payload-limit=2000000000
      - --max-client-batch-size=4096
      - --max-batch-tokens=131072
      - --dtype=float16
      - --auto-truncate
    ports:
      - "8083:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  multi-qa-MiniLM-L6-cos-v1:
    image: ghcr.io/huggingface/text-embeddings-inference:cpu-1.2
    command:
      - --model-id=sentence-transformers/multi-qa-MiniLM-L6-cos-v1
      - --payload-limit=2000000000
      - --max-client-batch-size=4096
      - --max-batch-tokens=131072
      - --dtype=float16
      - --auto-truncate
    ports:
      - "8084:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  ### rerankers
  bge-m3-reranker-v2:
    image: ghcr.io/huggingface/text-embeddings-inference:cpu-1.2
    command:
      - --model-id=BAAI/bge-reranker-v2-m3
      - --payload-limit=2000000000
      - --max-client-batch-size=4096
      - --max-batch-tokens=131072
      - --dtype=float16
      - --auto-truncate
    ports:
      - "8085:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  ### generation
  nous-research-hermes-2-pro-llama-3-8b:
    image: ghcr.io/huggingface/text-generation-inference:2.0
    command:
      - --model-id=NousResearch/Hermes-2-Pro-Llama-3-8B
      - --cuda-memory-fraction=0.75
      - --max-input-tokens=8191
      - --max-batch-prefill-tokens=8192
      - --max-total-tokens=8192
      - --max-batch-total-tokens=8192
      - --max-batch-size=8192
      - --dtype=bfloat16
      - --disable-custom-kernels
    ports:
      - "8087:80"
    volumes:
      - data:/data
    pull_policy: always
    platform: linux/amd64

  langfuse-server:
    image: ghcr.io/langfuse/langfuse:latest
    depends_on:
      - db
    ports:
      - "3000:3000"
    environment:
      - DATABASE_URL=postgresql://postgres:postgres@db:5432/postgres
      - NEXTAUTH_SECRET=mysecret
      - SALT=mysalt
      - NEXTAUTH_URL=http://localhost:3000
      - TELEMETRY_ENABLED=${TELEMETRY_ENABLED:-true}
      - LANGFUSE_ENABLE_EXPERIMENTAL_FEATURES=${LANGFUSE_ENABLE_EXPERIMENTAL_FEATURES:-false}

  db:
    image: postgres
    restart: always
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_DB=postgres
    ports:
      - 5432:5432
    volumes:
      - database_data:/var/lib/postgresql/data

  mongo:
    image: mongo
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: example
  
  milvus-standalone:
    image: milvusdb/milvus:v2.4.0
    container_name: milvus-standalone
    security_opt:
      - seccomp:unconfined
    environment:
      - ETCD_USE_EMBED=true
      - ETCD_DATA_DIR=/var/lib/milvus/etcd
      - ETCD_CONFIG_PATH=/milvus/configs/embedEtcd.yaml
      - COMMON_STORAGETYPE=local
    volumes:
      - milvus:/var/lib/milvus
      - ./configs/milvus_embedEtcd.yaml:/milvus/configs/embedEtcd.yaml
    ports:
      - "19530:19530"
      - "9091:9091"
      - "2379:2379"
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9091/healthz"]
      interval: 30s
      timeout: 20s
      retries: 3
      start_period: 90s
    command: ["milvus", "run", "standalone"]

  m3-server:
    build:
      context: .
      dockerfile: Dockerfile.m3
    ports:
      - 8881:3000

volumes:
  vespa_var:
  vespa_logs:
  data:
  models:
  database_data:
    driver: local
  milvus:
