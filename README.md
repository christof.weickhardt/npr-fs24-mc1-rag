# Mini Challenge Retrieval Augmented Generation (RAG)

## Abstract
The mini-challenge focuses on the development and evaluation of a Retrieval Augmented Generation (RAG) system. This system enhances the capabilities of a language model by retrieving information from external sources to provide accurate answers. Participants are tasked with implementing and testing various components such as ingestion, retrieval, and generation modules, and will evaluate their system's performance using both human measures and automatic methods. The goal is to deepen understanding of NLP tools and improve practical skills in developing and evaluating advanced language models.

## Table of Contents
- [Installation](#installation)
- [Project Structure](#project-structure)
- [Usage](#usage)
- [Data](#data)
- [Documentation](#documentation)
- [Experiments](#experiments)
- [Contributing](#contributing)
- [License](#license)
- [Authors and Acknowledgment](#authors-and-acknowledgment)
- [Contact Information](#contact-information)

## Installation
This project can be set up in a variety of environments. For details, refer to the specific environment setup files:
- **Python Dependencies**: `pip install -r requirements.txt`
- **Conda Environment**: `conda env create -f conda-env.yml`
- **Docker**:
  - For GPU: `docker-compose -f docker-compose.cuda.yml up`
  - For CPU: `docker-compose -f docker-compose.cpu.yml up`

## Project Structure
```
npr-fs24-mc1-rag/
├── data/
│ ├── processed/
│ │ └── classification/
│ └── raw/
├── docs/
├── experiments/
│ └── auto-eval/
├── export/
└── src/
```

## Usage
To get started with the RAG system:
1. Clone the repository and navigate into the directory.
2. Set up your environment using one of the above methods.
3. Start exploring the notebooks in the `experiments/` directory to understand different aspects of the technology.

## Data
- **Raw Data**: Stored in `data/raw/`, this includes the initial datasets.
- **Processed Data**: Intermediate data used in the system is stored in `data/processed/`, with subfolders for specific preprocessing tasks like classification.

## Documentation
Detailed documentation about the challenge and system components is available in the `docs/` directory.

## Experiments
Notebooks related to various experiments can be found under `experiments/`, including automated evaluation modules in `experiments/auto-eval/`.