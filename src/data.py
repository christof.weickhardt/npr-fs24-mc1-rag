# define function to build datasets, use df["content"] for doc input, then if is lambda function execute given BAAI/bge-m3 tokenizer or embeddings variable for embeddings
def build_dataset(chunker, df, tokenizer=None, embeddings=None):
    if callable(chunker):
        # check if name starts with semantic
        if embeddings:
            chunker = chunker(embeddings)
        else:
            chunker = chunker(tokenizer)
    return [(content, chunker.create_documents([content])) for content in tqdm(df["content"].tolist())]