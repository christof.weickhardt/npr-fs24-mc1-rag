
import requests
import pandas as pd
from tqdm import tqdm
import orjson  # Using orjson for fast JSON parsing and serialization

def m3_embed(url: str, sentences: str | List[str]):
    """
    Sends a list of sentences to a specified endpoint and returns the response.

    :param sentences: A list of sentences to be sent as data in the POST request.
    :return: A tuple containing the HTTP status code and the response text.
    """
    # URL for the POST request
    #url = 'http://weicki.internet-box.ch:8888/embeddings/'
    if isinstance(sentences, str):
        sentences = [sentences]
    # Data to be sent in JSON format, serialized with orjson for efficiency
    data = orjson.dumps({
        "sentences": sentences
    })

    # Set the headers
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
    }

    # Make the POST request
    response = requests.post(url, data=data, headers=headers)

    # Return the status code and response data
    return response.status_code, orjson.loads(response.text)