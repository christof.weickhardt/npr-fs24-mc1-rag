{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Text Cleaning For RAG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we're going to clean the data for our RAG.\n",
    "We'll use some tools to help us understand the data better:\n",
    "\n",
    "- `pandas` \n",
    "- `langid`\n",
    "- `string`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "source": [
    "import pandas as pd\n",
    "import langid\n",
    "import string"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading the Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset includes columns such as title, date, author, content, domain, and URL. Noticeable is the presence of NaN values in the `author` column and the raw format of the `content` field, which will require cleaning and normalization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "source": [
    "df = pd.read_csv('evaluation_generatequality/data/cleantech_media_dataset_v2_2024-02-23.csv')\n",
    "df"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Planned Cleaning Steps\n",
    "\n",
    "#### Handling Missing Values\n",
    "\n",
    "We will need to address missing values, particularly in the `author` column. Strategies will include assessing the impact of these missing values and deciding whether to fill them with placeholder data or remove the affected rows based on their proportion and significance.\n",
    "\n",
    "#### Content Column Examination\n",
    "\n",
    "The `content` column is crucial for our analysis. We intend to clean this column thoroughly to ensure that it is in a suitable format for text processing. This will involve normalizing text data, such as converting all text to the same case and removing extraneous punctuation.\n",
    "\n",
    "#### Language Verification\n",
    "\n",
    "It is essential to verify the language of each article. We plan to use the `langid` library to confirm that the content is in English, facilitating consistent processing and analysis.\n",
    "\n",
    "#### Removal of Special Characters\n",
    "\n",
    "We will remove special characters from the `content` column to reduce data noise and prepare it for advanced NLP tasks. This step will help in streamlining tokenization and vectorization processes later in our project.\n",
    "\n",
    "## Importance of Data Preparation for RAG\n",
    "\n",
    "Proper data preparation is a cornerstone of successful machine learning projects. \"Garbage in, garbage out\" holds especially true in the context of machine learning:\n",
    "\n",
    "- **Text Normalization:** By standardizing the text data, we ensure the LLM (Language Learning Model) is trained on data that represents meaningful textual information without the distraction of irrelevant formatting differences.\n",
    "- **Data Quality Assurance:** Continuously verifying and improving the quality of the data throughout the preparation phase is crucial. This ensures the model has the most accurate and relevant data to learn from.\n",
    "\n",
    "This preparation will form the backbone of our efforts to enhance RAG's capabilities, ensuring it can generate reliable and insightful outputs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Checking for NaN Values\n",
    "\n",
    "In this section, we address missing values in our dataset, which is crucial for maintaining data integrity and ensuring accurate analyses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify Missing Values\n",
    "\n",
    "First, we assess where missing values exist in our dataset using the `isnull().sum()` method. This provides a clear count of NaN values across different columns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "source": [
    "# show nan values\n",
    "df.isnull().sum()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `author` column contains a significant number of missing values (9562), suggesting that author information is frequently absent in the data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Handling Missing Data\n",
    "\n",
    "Given the high number of missing values in the `author` column and considering it may not be crucial for our analysis, we decide to remove this column. Similarly, the `Unnamed: 0` column, likely an index column from the original data file, is also redundant and removed:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "source": [
    "# remove author column\n",
    "df = df.drop(columns=['author'])\n",
    "df = df.drop(columns=['Unnamed: 0'])"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Date Column Transformation\n",
    "\n",
    "To facilitate time-series analyses or chronological ordering in future steps, we convert the `date` column to a datetime format. This standardizes the date entries and enables more complex functions like time-based filtering:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "source": [
    "# convert date column to datetime\n",
    "df['date'] = pd.to_datetime(df['date'])"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "source": [
    "df.head()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we display the first few entries of the updated dataset to confirm our changes. With these adjustments, our dataset is now better structured and prepared for more detailed analysis and modeling tasks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analyzing the Content Column\n",
    "\n",
    "This section focuses on processing the `content` column of our dataset to ensure the textual data is clean and uniform, which is critical for any subsequent natural language processing or textual analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Initial Content Review\n",
    "\n",
    "First, we examine the actual content stored in the dataset to understand its structure and format. This step is crucial for identifying any inconsistencies or peculiarities in the text data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "source": [
    " # print the content of the first row.\n",
    "df['content'][0]"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check for List Format\n",
    "\n",
    "We verify if any entries in the `content` column are stored as lists rather than plain strings, which could affect how we handle text processing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "source": [
    "# Check if in the 'content' column are multiple lists per entry\n",
    "df['content'].apply(lambda x: isinstance(x, list)).sum()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "source": [
    "df[\"content\"] = df[\"content\"].apply(eval)\n",
    "\n",
    "df = df.explode('content')"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "source": [
    "df.shape"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "source": [
    "(df[\"content\"].str.strip() == \"\").sum()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "source": [
    "df[(df[\"content\"].str.strip() == \"\")]"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "source": [
    "df = df[(df[\"content\"].str.strip() != \"\")]"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "source": [
    "df[df.duplicated()].sort_values(\"content\")"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "source": [
    "df = df[~df.duplicated()]"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result, `0`, confirms that no entries are stored as lists, ensuring uniformity in data format that allows for straightforward text operations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning Textual Anomalies\n",
    "\n",
    "To enhance readability and processing, we remove specific textual anomalies such as (`\\'\\'` and ` `` `) often used in the text for quoting. A crucial step in preparing text data for analysis is normalization. Lowercasing all text is a simple yet effective method to standardize the dataset and reduce complexity during analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "source": [
    "from ftfy import fix_text, fix_encoding\n",
    "from unstructured.cleaners.core import replace_unicode_quotes\n",
    "\n",
    "df[\"content\"] = df[\"content\"].apply(fix_encoding).apply(fix_text).apply(replace_unicode_quotes)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This transformation ensures that the same words in different cases are interpreted as identical, preventing duplication and reducing potential biases in text analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "source": [
    "print (df['content'][0])"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we display the updated content of the first entry to confirm our text manipulations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Language Verification of Content Column\n",
    "\n",
    "Given the global nature of our dataset, it's crucial to ensure that the content is consistently in English to maintain uniformity for any language-specific analysis or processing tasks. This section describes how we verify and standardize the language of the text data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Identifying Languages in the Dataset\n",
    "\n",
    "We use the `langid` library, which is a standalone language identification system, to classify the language of each entry in the `content` column. This step is essential to identify and address any language discrepancies in our data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "source": [
    "# TODO use real model to detect language\n",
    "# Check what languages the content column is written in\n",
    "#df[\"language\"] = df[\"content\"].apply(lambda x: langid.classify(x)[0])\n",
    "df[\"language\"].value_counts()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "df[df[\"language\"] == \"fr\"]"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "# print all languages that are not english in a list\n",
    "non_english = df[df[\"language\"] != \"en\"]\n",
    "non_english[\"language\"].unique()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "non_english.to_csv(\"data/cleantech_no_en.csv\", index=False)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "source": [
    "texts = [\n",
    "    [\n",
    "        \"[ 1 ] see for example: harvey et al 2021, larson et al. 2020, haley et al. 2019, larsen et al. 2019 and ipcc 2018.\"\n",
    "    ],\n",
    "    [\n",
    "        \"qatar petroleum ( qp) is targeting aggressive cuts in its greenhouse gas emissions as it prepares to launch phase 2 of its planned 48 million ton per year lng expansion. in its latest sustainability report published on wednesday, qp said its goals include  reducing the emissions intensity of qatar's lng facilities by 25% and of its upstream facilities by at least 15%.  the company is also aiming to reduce gas flaring intensity across its upstream facilities by more than 75% and has raised its carbon capture and storage ambitions from 5 million tons/yr to 7 million tons/yr by 2027. about 2.2 million tons/yr of the carbon capture goal will come from the 32 million ton/yr phase 1 of the lng expansion, also known as the north field east project. a further 1.1 million tons/yr will come from phase 2, known as the north field south project, which will raise qatar's lng capacity by a further 16 million tons/yr. qatar currently has an lng production capacity of around 78 million tons/yr and is eyeing a phased expansion to 126 million tons/yr. qp says it should be able to eliminate routine gas flaring by 2030, with methane emissions limited  by setting a methane intensity target of 0.2% across all facilities by 2025.  the company also plans to build some 1.6 gigawatts of solar energy capacity by 2025, half of which should come from the siraj solar power project next year ( eif jan.22'20). until this month, there had been little news about phase 2 of qatar's massive lng expansion. but mcdermott international said last week that it had been awarded the front-end engineering and design contract for five offshore wellhead platforms ( lngi jan.12'21). bids for construction of all four trains for phase 1 of the lng expansion were submitted in september ( lngi sep.15'20). but qp judged them to be too expensive and none met its targeted 50-week construction schedule. shortlisted contractors were asked to look for cost savings and submit new bids. the contract, which consultancy rystad estimates to be worth around $ 35 billion, is expected to be awarded by mar. 31. shortly after the construction contract is awarded, qp is expected to select foreign investments partners to take stakes of up to 30% in the phase 1 trains. exxon mobil, royal dutch shell, total, chevron, conocophillips and eni have been shortlisted. qp has repeatedly said that it is prepared to proceed without international investment partners if it determines that the offers it receives are not sufficiently attractive. but the shortlisted companies are expected to bid aggressively for what is expected to be the world's lowest-cost and most environmentally friendly lng ( lngi nov.9'20). rafiq latta, nicosia\"\n",
    "    ],\n",
    "    [\n",
    "        \"government actions in opposition to oil and gas introduce a range of potentially dangerous insecurities. we have been there, and done all of this before with oil and seen the consequences. that past experience, in large part, underlies notions of the criticality of minerals.\"\n",
    "    ],\n",
    "    [\n",
    "        \"a: we’ re not going to be anywhere near the pace and scale that we need to be in this clean energy transition. we need to accelerate even further with very robust, well thought-through government levers, funding streams, authorities and regulation, as well as private sector leadership.\"\n",
    "    ],\n",
    "    [\n",
    "        \"“ [ exploration is ] where there might be the best opportunity right now to really create some long-term substantial returns, because there’ s great opportunity, ” said apa ceo john christmann.\"\n",
    "    ],\n",
    "    [\n",
    "        \"“ i don't think it's going to be explicit, ” he said. “ i think it's not necessarily going to be the first or second thing. but probably the third [ or ] fourth thing. what we're observing is in the due diligence process, understanding if this deal is going to be accretive day one to the esg profile. ”\"\n",
    "    ],\n",
    "    [\n",
    "        \"ørsted has taken a final investment decision ( fid) on its first renewable hydrogen project, with plans to launch the facility later this year.\"\n",
    "    ],\n",
    "    [\n",
    "        \"gradient comfort – about us [ online ] available at: https: //www.gradientcomfort.com/pages/about-us\"\n",
    "    ],\n",
    "    [\"image credit: mariskavegter/shutterstock.com\"],\n",
    "    [\"related topics: carbon footprint renewable diesel utility\"],\n",
    "    [\"4. zero emissions power\"],\n",
    "    [\"برك المياه تقفل أوتوستراد # زوق مصبح # ملجق # لبنان pic.twitter.com/8njf85yq00\"],\n",
    "    [\"https: //t.co/kmucrzhy6z pic.twitter.com/ovbxoxseju\"],\n",
    "    [\"— patrick pouyanné ( @ ppouyanne) april 11, 2021\"],\n",
    "    [\n",
    "        \"( ofgem assume 2.9mwh per typical 🏠) ( ccc expect ⚡️increase for 🚗🚐🚛from 9 twh in 2022 to 47twh to 2030) https: //t.co/acoaky8tlj\"\n",
    "    ],\n",
    "    [\"ford mustang mach-e. photo by zach shahan | cleantechnica.\"],\n",
    "    [\"youtube: https: //www.youtube.com/c/bluettiofficial\"],\n",
    "    [\n",
    "        \"volkswagen id. buzz concept electric van, aka hippie bus. image courtesy of volkswagen.\"\n",
    "    ],\n",
    "    [\"issn © 1532-1231 | issn © 2577-9877 | issn © 1532-1266 |\"],\n",
    "    [\"your password *\"],\n",
    "    [\"reset password\"],\n",
    "    [\"decentralise\"],\n",
    "]"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "source": [
    "from transformers import pipeline\n",
    "\n",
    "classes_verbalized = [\n",
    "    \"Image Credit\",\n",
    "    \"Image Source\",\n",
    "    \"Email Address\",\n",
    "    \"Link\",\n",
    "    \"Text Blog\",\n",
    "    \"Text Paragraph\",\n",
    "    \"Text Report\",\n",
    "    \"Reference\",\n",
    "    \"Topic Tags\",\n",
    "    \"password\",\n",
    "]\n",
    "zeroshot_classifier_v3 = pipeline(\n",
    "    \"zero-shot-classification\", model=\"MoritzLaurer/deberta-v3-large-zeroshot-v2.0\"\n",
    ")\n",
    "zeroshot_classifier_m3 = pipeline(\n",
    "    \"zero-shot-classification\", model=\"MoritzLaurer/bge-m3-zeroshot-v2.0\"\n",
    ")\n",
    "\n",
    "# Initialize a list to store DataFrames\n",
    "df_v3 = []\n",
    "df_m3 = []\n",
    "\n",
    "# Process each text with both models\n",
    "for text in texts:\n",
    "    output_v3 = zeroshot_classifier_v3(text, classes_verbalized, multi_label=False)\n",
    "\n",
    "    # Create DataFrames from the outputs\n",
    "    df_output_v3 = pd.DataFrame(output_v3)\n",
    "\n",
    "    # Append each DataFrame to the list\n",
    "    df_v3.append(df_output_v3)\n",
    "\n",
    "    # add column \"model\"\n",
    "    df_output_v3[\"model\"] = \"v3\"\n",
    "\n",
    "# Process each text with both models\n",
    "for text in texts:\n",
    "    output_m3 = zeroshot_classifier_m3(text, classes_verbalized, multi_label=False)\n",
    "\n",
    "    # Create DataFrames from the outputs\n",
    "    df_output_m3 = pd.DataFrame(output_m3)\n",
    "\n",
    "    # Append each DataFrame to the list\n",
    "    df_m3.append(df_output_m3)\n",
    "\n",
    "    # add column \"model\"\n",
    "    df_output_m3[\"model\"] = \"m3\"\n",
    "\n",
    "# Concatenate all DataFrames in the list\n",
    "df_v3 = pd.concat(df_v3)\n",
    "df_m3 = pd.concat(df_m3)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "source": [
    "# Concatenate all DataFrames in the list\n",
    "df_v3.to_csv(\"data/v3.csv\", index=False)\n",
    "df_m3.to_csv(\"data/m3.csv\", index=False)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "source": [
    "df_v3 = pd.read_csv(\"data/processed/classification/v3.csv\")\n",
    "df_m3 = pd.read_csv(\"data/processed/classification/m3.csv\")"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "source": [
    "df_v3.info()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "source": [
    "import pandas as pd\n",
    "import ast\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Assuming df_v3 and df_m3 are already defined and contain the data as described\n",
    "\n",
    "\n",
    "# Function to convert string representation of lists into actual lists\n",
    "def str_to_list(s):\n",
    "    return ast.literal_eval(s)\n",
    "\n",
    "\n",
    "# Convert string representations in both DataFrames\n",
    "df_v3[\"labels\"] = df_v3[\"labels\"].apply(str_to_list)\n",
    "df_v3[\"scores\"] = df_v3[\"scores\"].apply(str_to_list)\n",
    "df_m3[\"labels\"] = df_m3[\"labels\"].apply(str_to_list)\n",
    "df_m3[\"scores\"] = df_m3[\"scores\"].apply(str_to_list)\n",
    "\n",
    "# Ensure both dataframes are aligned by sequence or by an appropriate key\n",
    "# This step assumes the sequences align perfectly and are in the same order.\n",
    "for idx in range(max(len(df_v3), len(df_m3))):\n",
    "    # Print the sequence\n",
    "    if idx < len(df_v3):\n",
    "        print(\"Sequence from df_v3:\", df_v3.iloc[idx][\"sequence\"])\n",
    "    elif idx < len(df_m3):  # If no matching sequence in df_v3\n",
    "        print(\"Sequence from df_m3:\", df_m3.iloc[idx][\"sequence\"])\n",
    "\n",
    "    plt.figure(figsize=(12, 6))  # Set the figure size for each pair of plots\n",
    "\n",
    "    # Plot for model v3 if available\n",
    "    if idx < len(df_v3):\n",
    "        labels_v3 = df_v3.iloc[idx][\"labels\"]\n",
    "        scores_v3 = df_v3.iloc[idx][\"scores\"]\n",
    "        model_v3 = df_v3.iloc[idx][\"model\"]\n",
    "\n",
    "        plt.subplot(1, 2, 1)  # Left plot for v3 model\n",
    "        plt.bar(labels_v3, scores_v3, color=\"blue\")\n",
    "        plt.title(f\"Model: {model_v3}\")\n",
    "        plt.xlabel(\"Labels\")\n",
    "        plt.ylabel(\"Scores\")\n",
    "        plt.xticks(rotation=45, ha=\"right\")\n",
    "\n",
    "    # Plot for model m3 if available\n",
    "    if idx < len(df_m3):\n",
    "        labels_m3 = df_m3.iloc[idx][\"labels\"]\n",
    "        scores_m3 = df_m3.iloc[idx][\"scores\"]\n",
    "        model_m3 = df_m3.iloc[idx][\"model\"]\n",
    "\n",
    "        plt.subplot(1, 2, 2)  # Right plot for m3 model\n",
    "        plt.bar(labels_m3, scores_m3, color=\"green\")\n",
    "        plt.title(f\"Model: {model_m3}\")\n",
    "        plt.xlabel(\"Labels\")\n",
    "        plt.ylabel(\"Scores\")\n",
    "        plt.xticks(rotation=45, ha=\"right\")\n",
    "\n",
    "    plt.tight_layout(\n",
    "        rect=[0, 0.03, 1, 0.95]\n",
    "    )  # Adjust layout to make room for the main title\n",
    "    plt.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "source": [
    "import ast\n",
    "\n",
    "\n",
    "# Function to convert string representations of lists into actual lists\n",
    "def str_to_list(s):\n",
    "    return ast.literal_eval(s)\n",
    "\n",
    "\n",
    "# Apply conversions\n",
    "df_v3[\"labels\"] = df_v3[\"labels\"].apply(str_to_list)\n",
    "df_v3[\"scores\"] = df_v3[\"scores\"].apply(str_to_list)\n",
    "df_v3[\"model\"] = df_v3[\"model\"].astype(str)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Extracting the first row for plotting\n",
    "labels = df_v3.iloc[0][\"labels\"]\n",
    "scores = df_v3.iloc[0][\"scores\"]\n",
    "model = df_v3.iloc[0][\"model\"]\n",
    "\n",
    "# Plotting\n",
    "plt.figure(figsize=(12, 6))\n",
    "plt.bar(labels, scores, color=\"blue\")\n",
    "plt.xlabel(\"Labels\")\n",
    "plt.ylabel(\"Scores\")\n",
    "plt.title(f\"Model: {model}\")\n",
    "plt.xticks(rotation=45, ha=\"right\")  # Rotate labels to avoid overlap\n",
    "plt.tight_layout()  # Adjust layout to make room for label rotation\n",
    "plt.show()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "import ast\n",
    "\n",
    "# Convert 'labels' column to list\n",
    "df_v3[\"labels\"] = df_v3[\"labels\"].apply(ast.literal_eval)\n",
    "\n",
    "# Convert 'scores' column to list\n",
    "df_v3[\"scores\"] = df_v3[\"scores\"].apply(ast.literal_eval)\n",
    "\n",
    "# Convert 'model' column to string\n",
    "df_v3[\"model\"] = df_v3[\"model\"].astype(str)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "print(df_v3[\"labels\"][0])\n",
    "print(df_v3[\"scores\"][0])"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "# make each label a column and add the corresponding score\n",
    "df_v3 = pd.concat(\n",
    "    [\n",
    "        df_v3.drop([\"labels\", \"scores\"], axis=1),\n",
    "        df_v3[\"labels\"]\n",
    "        .apply(pd.Series)\n",
    "        .merge(df_v3[\"scores\"].apply(pd.Series), left_index=True, right_index=True),\n",
    "    ],\n",
    "    axis=1,\n",
    ")\n",
    "\n",
    "df_v3.head()"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "# check types of each column\n",
    "df_v3.dtypes"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "# remove non-english entries\n",
    "df = df[df[\"language\"] == \"en\"]\n",
    "#df[\"language\"] = df[\"content\"].apply(lambda x: langid.classify(x)[0])\n",
    "df[\"language\"].value_counts()"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output shows the dominance of English (`en`) in our dataset, along with a few entries in German (`de`), Russian (`ru`), and Spanish (`es`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following the removal of non-English entries, we perform another count to ensure no non-English entries remain.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following the removal of non-English entries, we perform another count to ensure no non-English entries remain. This confirms that our dataset is now exclusively composed of English entries, with a total of 9587 entries remaining."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a final step, since we have confirmed that all entries are in English and the language column is no longer needed, we remove it to streamline our dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing Special Characters from Content\n",
    "\n",
    "In this section, we focus on identifying and removing unnecessary special characters from the `content` column of our dataset. This step is crucial for cleaning the text data, which can improve the quality of our text-based analyses and machine learning processes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Identifying Special Characters\n",
    "\n",
    "First, we generate a list of all special characters present in the `content` by iterating through each character in each string. This helps us see which characters are potentially disruptive or irrelevant for our analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "source": [
    "# print a list of all special characters that are in the content column\n",
    "special_characters = []\n",
    "for i in df['content']:\n",
    "    for j in i:\n",
    "        if j in string.punctuation:\n",
    "            special_characters.append(j)\n",
    "            \n",
    "print(set(special_characters))"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output reveals a wide range of special characters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cleaning Text Data\n",
    "\n",
    "To clean our text data, we decide to remove most special characters while preserving a select few that might be important for understanding the text, such as periods (.), exclamation marks (!), question marks (?), slashes (/), dollar signs ($), percent signs (%), commas (,), and brackets ([])."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "source": [
    "df['content'].iloc[20:40]"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "source": [
    "# remove special characters but not (. ! ? / $ % , [])\n",
    "df['content'] = df['content'].str.replace('[^a-zA-Z0-9\\s.!?/$%,\\[\\]]', '', regex=True)\n",
    "\n",
    "special_characters = []\n",
    "for i in df['content']:\n",
    "    for j in i:\n",
    "        if j in string.punctuation:\n",
    "            special_characters.append(j)\n",
    "            \n",
    "print(set(special_characters))"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "source": [
    "from unstructured.cleaners import core\n",
    "from functools import partial\n",
    "\n",
    "df[\"content\"] = df[\"content\"].apply(partial(core.clean, extra_whitespace=True, dashes=True, bullets=True))"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "source": [
    "df[\"content\"].apply(cleantext.clean)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This regular expression allows us to keep these essential characters, removing all other forms of punctuation that could interfere with textual analysis or processing. The result shows a reduced set of special characters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Verify Removal of Unwanted Characters\n",
    "\n",
    "After cleaning, we check again to confirm that only our selected special characters remain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "print(df['content'][0])"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Saving the Cleaned Dataset\n",
    "\n",
    "Finally, we save our cleaned dataset to a new CSV file, preserving our changes for further analysis or sharing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "# save df as clean_cleantech.csv in the data folder\n",
    "df.to_parquet('data/clean_cleantech.parquet', index=False)"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This file, `clean_cleantech.csv`, now contains the cleaned content ready for further processing or analysis in future steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "source": [
    "# Chunking in different strategies\n",
    "from langchain_text_splitters import RecursiveCharacterTextSplitter\n",
    "from langchain_text_splitters import CharacterTextSplitter\n",
    "from transformers import AutoTokenizer\n",
    "from langchain_experimental.text_splitter import SemanticChunker\n",
    "\n",
    "\n",
    "def build_char_splitter(tokenizer_name, chunk_size, overlap):\n",
    "    tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)\n",
    "    return CharacterTextSplitter.from_huggingface_tokenizer(\n",
    "        tokenizer, chunk_size=chunk_size, chunk_overlap=overlap\n",
    "    )\n",
    "\n",
    "\n",
    "chunkers = {\n",
    "    \"recursive_1k_0.2\": RecursiveCharacterTextSplitter(\n",
    "        # Set a really small chunk size, just to show.\n",
    "        chunk_size=1024,\n",
    "        chunk_overlap=20,\n",
    "        length_function=len,\n",
    "        is_separator_regex=False,\n",
    "    ),\n",
    "    \"recursive_1k_0.1\": RecursiveCharacterTextSplitter(\n",
    "        # Set a really small chunk size, just to show.\n",
    "        chunk_size=1024,\n",
    "        chunk_overlap=10,\n",
    "        length_function=len,\n",
    "        is_separator_regex=False,\n",
    "    ),\n",
    "    \"recursive_0.5k_0.1\": RecursiveCharacterTextSplitter(\n",
    "        # Set a really small chunk size, just to show.\n",
    "        chunk_size=512,\n",
    "        chunk_overlap=10,\n",
    "        length_function=len,\n",
    "        is_separator_regex=False,\n",
    "    ),\n",
    "    \"recursive_0.2k_0.1\": RecursiveCharacterTextSplitter(\n",
    "        # Set a really small chunk size, just to show.\n",
    "        chunk_size=256,\n",
    "        chunk_overlap=10,\n",
    "        length_function=len,\n",
    "        is_separator_regex=False,\n",
    "    ),\n",
    "    \"windowed_0.1k_0.0\": lambda tokenizer_name: build_char_splitter(\n",
    "        tokenizer_name, 128, 0\n",
    "    ),\n",
    "    \"windowed_0.1k_0.1\": lambda tokenizer_name: build_char_splitter(\n",
    "        tokenizer_name, 128, 0.1\n",
    "    ),\n",
    "    \"windowed_0.2k_0.0\": lambda tokenizer_name: build_char_splitter(\n",
    "        tokenizer_name, 256, 0\n",
    "    ),\n",
    "    \"windowed_0.2k_0.1\": lambda tokenizer_name: build_char_splitter(\n",
    "        tokenizer_name, 256, 0.1\n",
    "    ),\n",
    "    \"windowed_0.5k_0.0\": lambda tokenizer_name: build_char_splitter(\n",
    "        tokenizer_name, 512, 0\n",
    "    ),\n",
    "    \"windowed_0.5k_0.1\": lambda tokenizer_name: build_char_splitter(\n",
    "        tokenizer_name, 512, 0.1\n",
    "    ),\n",
    "    \"semantic_percentile\": lambda embedding_model: SemanticChunker(\n",
    "        embedding_model, breakpoint_threshold_type=\"percentile\"\n",
    "    ),\n",
    "    \"semantic_standard_deviation\": lambda embedding_model: SemanticChunker(\n",
    "        embedding_model, breakpoint_threshold_type=\"standard_deviation\"\n",
    "    ),\n",
    "    \"semantic_interquartile\": lambda embedding_model: SemanticChunker(\n",
    "        embedding_model, breakpoint_threshold_type=\"interquartile\"\n",
    "    ),\n",
    "}"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "source": [
    "from langchain_community.embeddings import HuggingFaceHubEmbeddings\n",
    "from tqdm import tqdm\n",
    "\n",
    "#embeddings = HuggingFaceHubEmbeddings(model=\"http://209.20.157.39:8080\")"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "source": [
    "# define function to build datasets, use df[\"content\"] for doc input, then if is lambda function execute given BAAI/bge-m3 tokenizer or embeddings variable for embeddings\n",
    "def build_dataset(chunker, df, tokenizer=None, embeddings=None):\n",
    "    if callable(chunker):\n",
    "        # check if name starts with semantic\n",
    "        if embeddings:\n",
    "            chunker = chunker(embeddings)\n",
    "        else:\n",
    "            chunker = chunker(tokenizer)\n",
    "    return [(chunk, chunker.create_documents([chunk])) for chunk in tqdm(df[\"content\"].tolist())]"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "source": [
    "from multiprocessing.pool import ThreadPool\n",
    "from tqdm import tqdm\n",
    "\n",
    "def build_datasets(chunkers, df, models):\n",
    "    # Function to process each chunker-model pair\n",
    "    def process_task(task):\n",
    "        name, chunker, tokenizer_name, model = task  # Unpack the task tuple\n",
    "        print(model)\n",
    "        # Ensure proper conditions before processing\n",
    "        if \"recursive\" not in name and \"semantic\" in name:\n",
    "            # Call the dataset building function\n",
    "            result = build_dataset(chunker, df, tokenizer=tokenizer_name if \"semantic\" not in name else None, embeddings=model if \"semantic\" in name else None)\n",
    "            return (name, result)\n",
    "        return None\n",
    "\n",
    "    # Prepare the list of tasks with all combinations\n",
    "    tasks = [(name, chunker, tokenizer_name, model) for name, chunker in chunkers.items()\n",
    "             for tokenizer_name, model in models.items() if \"recursive\" not in name and \"semantic\" in name]\n",
    "\n",
    "    # Create a ThreadPool and process the tasks\n",
    "    with ThreadPool() as pool:\n",
    "        results = list(tqdm(pool.imap_unordered(process_task, tasks), total=len(tasks)))\n",
    "\n",
    "    # Combine results from all threads\n",
    "    ds = {}\n",
    "    for result in results:\n",
    "        if result:\n",
    "            name, dataset = result\n",
    "            if name in ds:\n",
    "                ds[name].update(dataset)  # Merge datasets if multiple results per name\n",
    "            else:\n",
    "                ds[name] = dataset\n",
    "\n",
    "    return ds"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "source": [
    "datasets = build_datasets(chunkers, df, embeddings_models)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "source": [
    "model_endpoints = {\n",
    "    \"BAAI/bge-m3\": \"http://209.20.157.39:8080\",\n",
    "    \"setu4993/LaBSE\": \"http://209.20.157.39:8081\",\n",
    "    \"nomic-ai/nomic-embed-text-v1.5\": \"http://209.20.157.39:8082\",\n",
    "    \"sentence-transformers/distiluse-base-multilingual-cased\": \"http://209.20.157.39:8083\",\n",
    "    \"sentence-transformers/multi-qa-MiniLM-L6-cos-v1\": \"http://209.20.157.39:8084\"\n",
    "}\n",
    "\n",
    "embeddings_models = {\n",
    "    #\"BAAI/bge-m3\": HuggingFaceHubEmbeddings(model=model_endpoints[\"BAAI/bge-m3\"]),\n",
    "    #\"setu4993/LaBSE\": HuggingFaceHubEmbeddings(model=model_endpoints[\"setu4993/LaBSE\"]),\n",
    "    #\"nomic-ai/nomic-embed-text-v1.5\": HuggingFaceHubEmbeddings(model=model_endpoints[\"nomic-ai/nomic-embed-text-v1.5\"]),\n",
    "    \"sentence-transformers/distiluse-base-multilingual-cased\": HuggingFaceHubEmbeddings(model=model_endpoints[\"sentence-transformers/distiluse-base-multilingual-cased\"]),\n",
    "    \"sentence-transformers/multi-qa-MiniLM-L6-cos-v1\": HuggingFaceHubEmbeddings(model=model_endpoints[\"sentence-transformers/multi-qa-MiniLM-L6-cos-v1\"])\n",
    "    # TODO openai embeddings\n",
    "}"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [],
   "outputs": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
